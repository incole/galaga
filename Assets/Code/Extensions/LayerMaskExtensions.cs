﻿using UnityEngine;

public static class LayerMaskExtensions
{
    public static bool ContainsLayer(this LayerMask layermask, int layer)
    {
        return layermask == (layermask | (1 << layer));
    }
}
