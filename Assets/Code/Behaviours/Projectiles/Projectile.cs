﻿using System;
using UnityEngine;
using UnityEngine.Pool;

[RequireComponent(typeof(Move))]
public abstract class Projectile : MonoBehaviour
{
    public Move Move;
    public int Damage = 1;
    public LayerMask HitboxMask;

    internal SpaceShip _initiator;

    public abstract event Action<Projectile> OnHit;

    public abstract void LaunchInDirection(SpaceShip initiator, Vector3 launchPosition, Vector3 direction);
    public abstract void LaunchToTarget(SpaceShip initiator, Vector3 launchPosition, Transform target);
    internal abstract void Finish();

    private void Reset()
    {
        Move = GetComponent<Move>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // Hit not hitbox
        if (!HitboxMask.ContainsLayer(other.gameObject.layer))
        {
            Finish();
            return;
        }

        var hitbox = other.GetComponent<Hitbox>();

        if (hitbox == null)
        {
            Debug.LogError("Hitbox without component", other);
            Finish();
            return;
        }

        // Self-hit
        if (hitbox.IsThisInitiator(_initiator))
            return;

        hitbox.ApplyDamage(Damage);
        Finish();
    }
}
