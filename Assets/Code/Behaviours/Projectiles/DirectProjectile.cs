﻿using System;
using UnityEngine;

public class DirectProjectile : Projectile
{
    public override event Action<Projectile> OnHit;

    public override void LaunchInDirection(SpaceShip initiator, Vector3 launchPosition, Vector3 direction)
    {
        _initiator = initiator;

        Move.transform.position = launchPosition;
        Move.Rigidbody.position = launchPosition;
        Move.transform.right = direction;
        Move.Direction = direction;
    }

    public override void LaunchToTarget(SpaceShip initiator, Vector3 launchPosition, Transform target)
    {
        _initiator = initiator;

        Move.transform.position = launchPosition;
        Move.Rigidbody.position = launchPosition;

        var direction = target.position - launchPosition;

        Move.transform.right = direction;
        Move.Direction = direction;
    }

    internal override void Finish()
    {
        OnHit?.Invoke(this);
    }
}
