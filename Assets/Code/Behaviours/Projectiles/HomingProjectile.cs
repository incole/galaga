﻿using System;
using UnityEngine;

public class HomingProjectile : Projectile
{
    public AutoAim Aim;
    public TrailRenderer TrailRenderer;

    public override event Action<Projectile> OnHit;

    public override void LaunchInDirection(SpaceShip initiator, Vector3 launchPosition, Vector3 direction)
    {
        Debug.LogError("Trying launch homing missle with direct method");
        Destroy(gameObject);
    }

    public override void LaunchToTarget(SpaceShip initiator, Vector3 launchPosition, Transform target)
    {
        _initiator = initiator;

        Aim.Target = target;

        Move.transform.position = launchPosition;
        Move.Rigidbody.position = launchPosition;

        var direction = target.position - launchPosition;

        Move.transform.right = direction;
        Move.Direction = direction;

        Aim.enabled = true;

        if (TrailRenderer != null)
            TrailRenderer.Clear();
    }

    internal override void Finish()
    {
        OnHit?.Invoke(this);
    }
}
