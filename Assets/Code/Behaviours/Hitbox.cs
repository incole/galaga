﻿using UnityEngine;

public class Hitbox: MonoBehaviour
{
    private SpaceShip _ship;

    private void Awake()
    {
        _ship = GetComponentInParent<SpaceShip>();

        if (_ship != null)
            return;

        Debug.LogError("Hitbox without ship", this);
        Destroy(this);
    }

    public bool IsThisInitiator(SpaceShip spaceShip)
    {
        return _ship == spaceShip;
    }

    public void ApplyDamage(int value)
    {
        _ship.ApplyDamage(value);
    }
}
