﻿using Cysharp.Threading.Tasks;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Vector2 Cooldown = new(1, 1);
    public bool IsCooling;

    public Transform Muzzle;

    public ProjectilePool ProjectilePool;

    private void Start()
    {
        if (ProjectilePool != null)
            return;
        
        Debug.LogError("Projectile is not set", this);
        Destroy(this);
    }

    public async void Fire(SpaceShip initiator, Vector3 target)
    {
        if (IsCooling)
            return;

        IsCooling = true;

        var projectile = ProjectilePool.Get();
        projectile.OnHit += Projectile_OnHit;

        projectile.LaunchInDirection(initiator, Muzzle.position, target);

        await UniTask.Delay(Mathf.CeilToInt(Random.Range(Cooldown.x, Cooldown.y) * 1000));

        IsCooling = false;
    }

    public async void Fire(SpaceShip initiator, Transform target)
    {
        if (IsCooling)
            return;

        IsCooling = true;

        var projectile = ProjectilePool.Get();
        projectile.OnHit += Projectile_OnHit;

        projectile.LaunchToTarget(initiator, Muzzle.position, target);

        await UniTask.Delay(Mathf.CeilToInt(Random.Range(Cooldown.x, Cooldown.y) * 1000));

        IsCooling = false;
    }

    private void Projectile_OnHit(Projectile obj)
    {
        obj.OnHit -= Projectile_OnHit;
        ProjectilePool.Release(obj);
    }
}
