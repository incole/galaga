using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    [ReadOnly]
    public SpaceShip SpaceShip;

    [ReadOnly]
    public Move Move;

    [ReadOnly]
    public Weapon Weapon;

    public void Initialize(SpaceShip playerShip)
    {
        SpaceShip = playerShip;
        Move = playerShip.GetComponent<Move>();
        Weapon = playerShip.GetComponent<Weapon>();
    }

    void Update()
    {
        if (Move == null)
            return;

        if (!Move.gameObject.activeInHierarchy)
            return;

        Move.Direction = new Vector3
        (
            Input.GetAxisRaw("Vertical"),
            0,
            -Input.GetAxisRaw("Horizontal")
        );

        if (Input.GetKey(KeyCode.Space))
        {
            Weapon.Fire(SpaceShip, Weapon.transform.right);
        }
    }
}
