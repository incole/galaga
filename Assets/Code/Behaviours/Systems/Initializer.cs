﻿using UnityEngine;

public class Initializer : MonoBehaviour
{
    public InitilizableScriptable[] Items;

    public void Awake()
    {
        for (int i = 0; i < Items.Length; i++)
            Items[i].Initialize();
    }
}