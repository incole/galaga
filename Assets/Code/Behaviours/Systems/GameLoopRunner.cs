﻿using UnityEngine;

public class GameLoopRunner: MonoBehaviour
{
    public Controller[] Controllers;
    public GameLoopRunnerType Type;

    private void Start()
    {
        for (int i = 0; i < Controllers.Length; i++)
            Controllers[i].Init();
    }

    private void Update()
    {
        if (Type != GameLoopRunnerType.Update)
            return;

        for (int i = 0; i < Controllers.Length; i++)
            Controllers[i].Update();
    }

    private void FixedUpdate()
    {
        if (Type != GameLoopRunnerType.FixedUpdate)
            return;

        for (int i = 0; i < Controllers.Length; i++)
            Controllers[i].Update();
    }
}

public enum GameLoopRunnerType
{
    Update,
    FixedUpdate
}