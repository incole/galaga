﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// HUD should be separated from here

public class GameplayController: MonoBehaviour
{
    public Camera Camera;
    public Transform PlayerSpawnPoint;

    [Space]
    public Transform EnemyGridRightCorner;
    public Transform EnemyGridLeftCorner;

    [Space]
    public Transform[] EnemySpawnPoints;
    public Transform[] EnemyPathPoints;

    [Space]
    public float EnemyGridStep;
    public int EnemyGridRows = 5;
    public int MinEnemiesPerPhase = 10;
    public int MaxEnemiesPerPhase = 100;
    public float PhaseCooldown = 2f;

    [Space]
    public int MaxEnemiesPerStage = 20;
    public float SpawnCooldown = 0.3f;
    public float StageCooldown = 1f;


    private Vector3[,] _enemyGridPositions;

    [Space]
    public SpaceShipPool PlayerShipPools;
    public SpaceShipPool[] EnemyShipPools;
    public PlayerInputController PlayerInput;

    public int CurrentStage = 1;
    public int Score = 0; //Likely to be separated
    public int PlayerLives = 3;
    public int Phase = 0;

    [Space]
    public float PlayerRespawnCooldown = 2f;
    public float PlayerRespawnInvincibilityTime = 1f;

    [Header("UI")]
    public UIRoundFinishedScreen RoundFinishedScreen;
    public Text ScoreField;
    public Text LivesField;
    public UIFader HUD;

    private int _currentEnemyCount;
    private int _playerCurrentLives;

    private SpaceShip _playerShip;

    private bool _spawnInProgress;
    private bool _gameIsEnded;

    private Dictionary<SpaceShip, SpaceShipPool> _liveShips;

    private void Start()
    {
        PrepareEnemyGrid();

        _liveShips = new Dictionary<SpaceShip, SpaceShipPool>();
    }

    private void PrepareEnemyGrid()
    {
        var enemyLeftTopPosition = EnemyGridLeftCorner.position + Vector3.left * EnemyGridStep * 2 + Vector3.back * EnemyGridStep;
        var enemyRightTopPosition = EnemyGridRightCorner.position + Vector3.left * EnemyGridStep * 2 + Vector3.forward * EnemyGridStep;

        var columnsCount = Mathf.RoundToInt((enemyRightTopPosition - enemyLeftTopPosition).magnitude / EnemyGridStep);

        _enemyGridPositions = new Vector3[EnemyGridRows, columnsCount];

        for (int i = 0; i < EnemyGridRows; i++)
            for (int j = 0; j < columnsCount; j++)
                _enemyGridPositions[i, j] = enemyLeftTopPosition + EnemyGridStep * i * Vector3.left + j * EnemyGridStep * Vector3.back;
    }

    public void RestartGameplay()
    {
        _spawnInProgress = false;
        _currentEnemyCount = 0;
        _gameIsEnded = false;

        Phase = 0;

        ApplyScore(0);
        ApplyLives(PlayerLives);

        // Don't ever again use unity default pools

        foreach (var ship in _liveShips.Keys)
        {
            var pool = _liveShips[ship];
            pool.Release(ship);
        }

        _liveShips.Clear();

        RespawnPlayer();
        ProcessPhase();

        PlayerInput.Initialize(_playerShip);
        PlayerInput.enabled = true;

        HUD.FadeInAsync();
    }

    private async void ProcessPlayerDeath(SpaceShip instance)
    {
        ApplyLives(_playerCurrentLives - 1);

        instance.OnDie -= ProcessPlayerDeath;
        PlayerShipPools.Release(instance);

        if (_playerCurrentLives == 0)
        {
            ProcessGameEnd();
            return;
        }

        PlayerInput.enabled = false;

        await UniTask.Delay(Mathf.CeilToInt(PlayerRespawnCooldown * 1000));

        while (!enabled)
            await UniTask.NextFrame();

        RespawnPlayer();

        PlayerInput.Initialize(_playerShip);
        PlayerInput.enabled = true;
    }

    private void ProcessGameEnd()
    {
        _gameIsEnded = true;
        PlayerInput.enabled = false;
        RoundFinishedScreen.Show(Score);
        HUD.FadeOutAsync();
    }

    private void RespawnPlayer()
    {
        _playerShip = PlayerShipPools.Get();

        _playerShip.Revive();

        _playerShip.Move.transform.position = PlayerSpawnPoint.transform.position;
        _playerShip.Move.Rigidbody.position = PlayerSpawnPoint.transform.position;
        _playerShip.Move.Rigidbody.velocity = Vector3.zero;

        _playerShip.OnDie += ProcessPlayerDeath;
        _playerShip.SetInvincible(PlayerRespawnInvincibilityTime);

        PlayerInput.SpaceShip = _playerShip;
        PlayerInput.Move = _playerShip.Move;
        PlayerInput.Weapon = _playerShip.Weapon;
    }

    private async void ProcessPhase()
    {
        if (_spawnInProgress)
            return;

        Phase++;

        _spawnInProgress = true;

        await UniTask.Delay(Mathf.CeilToInt(1000 * PhaseCooldown));

        while (!enabled)
            await UniTask.NextFrame();

        var endPointI = 0;
        var endPointJ = 0;

        var enemiesThisPhase = Random.Range(MinEnemiesPerPhase, MaxEnemiesPerPhase);

        while (enemiesThisPhase > 0)
        {
            var spawnPoint = EnemySpawnPoints[Random.Range(0, EnemySpawnPoints.Length)].position;
            var pathPoint = EnemyPathPoints[Random.Range(0, EnemyPathPoints.Length)].position;

            SpaceShipPool enemyShipPool = null;

            switch (Phase) //should be in editor settings
            {
                case 1:
                    enemyShipPool = EnemyShipPools[0];
                    break;

                case 2:
                    enemyShipPool = EnemyShipPools[1];
                    break;

                default:
                    enemyShipPool = EnemyShipPools[Random.Range(0, EnemyShipPools.Length)];
                    break;
            }

            for (int i = 0; i < MaxEnemiesPerStage; i++)
            {
                var endPoint = _enemyGridPositions[endPointI, endPointJ];

                endPointJ++;

                if (endPointJ >= _enemyGridPositions.GetLength(1))
                {
                    endPointI++;
                    endPointJ = 0;
                }

                var enemyShip = enemyShipPool.Get();
                enemyShip.Revive();
                enemyShip.OnDie -= ProcessEnemyDeath;
                enemyShip.OnDie += ProcessEnemyDeath;

                _liveShips[enemyShip] = enemyShipPool;

                var enemy = enemyShip.GetComponent<Enemy>(); //todo: make abstract ship, set enemy as reference to EnemyShip
                enemy.enabled = false;
                enemy.Target = _playerShip.transform;

                var enemyShipRoute = enemy.Route;
                enemyShipRoute.SetPath(new[] { pathPoint, endPoint });
                enemyShipRoute.enabled = true;

                var enemyShipMove = enemy.Route.Move;
                enemyShipMove.transform.forward = pathPoint - enemyShipMove.transform.position;
                enemyShipMove.transform.position = spawnPoint;
                enemyShipMove.Rigidbody.position = spawnPoint;
                enemyShipMove.enabled = true;

                enemiesThisPhase--;
                _currentEnemyCount++;

                if (enemiesThisPhase == 0)
                    break;

                await UniTask.Delay(Mathf.CeilToInt(1000 * SpawnCooldown));

                while (!enabled)
                    await UniTask.NextFrame();
            }
        }

        _spawnInProgress = false;
    }

    private void ProcessEnemyDeath(SpaceShip instance)
    {
        _currentEnemyCount--;

        instance.OnDie -= ProcessEnemyDeath;
        ApplyScore(Score + instance.Score);

        var pool = _liveShips[instance];
        pool.Release(instance);
        _liveShips.Remove(instance);

        if (_currentEnemyCount == 0)
            ProcessPhase();
    }

    private void ApplyScore(int value)
    {
        if (_gameIsEnded)
            return;

        Score = value;
        ScoreField.text = $"Score: {Score}";
    }

    private void ApplyLives(int value)
    {
        _playerCurrentLives = value;
        LivesField.text = $"Lives: {_playerCurrentLives}";
    }
}
