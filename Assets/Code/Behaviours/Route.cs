﻿using System;
using UnityEngine;

[RequireComponent(typeof(Move))]
public class Route : MonoBehaviour, IControllable
{
    public int Id { get; set; }

    [ReadOnly]
    public Move Move;

    [ReadOnly]
    public Vector3[] Path;

    public int CurrentTargetIndex;

    public event Action<Route> OnDestinationAchived;

    private void Reset()
    {
        CurrentTargetIndex = 0;
        Move = GetComponent<Move>();
    }

    private void Awake()
    {
        if (Move == null)
            Move = GetComponent<Move>();
    }

    public void SetPath(Vector3[] value)
    {
        Path = value;
        CurrentTargetIndex = 0;
    }

    public void Arrived()
    {
        OnDestinationAchived?.Invoke(this);
    }
}
