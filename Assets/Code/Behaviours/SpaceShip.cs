﻿using Cysharp.Threading.Tasks;
using System;
using UnityEngine;

public class SpaceShip : MonoBehaviour, IControllable
{
    public int MaxHealth = 1;
    public int Health = 1;
    public int Score = 100;
    public bool IsInvincible = false;

    public VFXPool DeathEffect;
    public Move Move;
    public Weapon Weapon;
    public Route Route;

    public int Id { get; set; }

    public event Action<SpaceShip> OnDie;

    public void Revive()
    {
        Health = MaxHealth;
    }

    public void ApplyDamage(int value)
    {
        if (IsInvincible)
            return;

        Health -= value;

        if (Health <= 0)
            Die();
    }

    public async void SetInvincible(float timeFrame)
    {
        IsInvincible = true;

        await UniTask.Delay(Mathf.CeilToInt(timeFrame * 1000));

        IsInvincible = false;
    }

    public void Die()
    {
        if (DeathEffect != null)
        {
            var effect = DeathEffect.Get();

            effect.OnParticleSystemFinished += OnVFXEnded;

            effect.transform.position = transform.position;
            effect.ParticleSystem.Play();
        }

        OnDie?.Invoke(this);
    }

    private void OnVFXEnded(ParticleSystemWrapper obj)
    {
        obj.OnParticleSystemFinished -= OnVFXEnded;
        DeathEffect.Release(obj);
    }
}
