﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Controllers/Auto Aim")]
public class AutoAimController : Controller<AutoAim>
{
    public override void Update()
    {
        for (int i = 0; i < _items.Count; i++)
        {
            var aim = _items[i];

            if (!aim.enabled)
                continue;

            var direction = aim.Target.position - aim.transform.position;

            if (Vector3.Angle(direction, aim.transform.right) > aim.MaxAngle)
            {
                aim.enabled = false;
                continue;
            }

            aim.Move.Direction = direction;
            aim.Move.SqrDistanceToDestination = direction.sqrMagnitude;
        }
    }
}