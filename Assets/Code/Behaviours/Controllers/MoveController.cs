﻿using UnityEngine;

[CreateAssetMenu(menuName = "Controllers/Move")]
public class MoveController: Controller<Move>
{
    public override void Update()
    {
        var deltaTime = Time.deltaTime;

        for (int i = 0; i < _items.Count; i++)
        {
            var move = _items[i];

            if (!move.enabled)
                continue;

            if (move.RotationSpeedOverDistance.length > 1)
            {
                var rotationSpeed = move.RotationSpeedOverDistance.Evaluate(move.SqrDistanceToDestination);
                move.transform.right = Vector3.Lerp(move.transform.right, move.Direction, rotationSpeed * deltaTime);
            }

            ProcessVelocity(move, deltaTime);
        }
    }

    private void ProcessVelocity(Move move, float deltaTime)
    {
        Vector3 velocity;

        if (move.Direction.sqrMagnitude == 0)
            if (move.Rigidbody.velocity.sqrMagnitude == 0)
                return;
            else
                velocity = GetDecelerationVelocity(move, deltaTime);
        else
        {
            switch (move.Type)
            {
                case MoveType.Forward:
                    velocity = GetForwardAccelerationVelocity(move, deltaTime);
                    break;

                case MoveType.Directional:
                    velocity = GetDirectionalAccelerationVelocity(move, deltaTime);
                    break;

                default:
                    return;
            }
        }

        move.PreviousPosition = move.Rigidbody.position;
        move.Rigidbody.velocity = velocity;
    }

    private Vector3 GetForwardAccelerationVelocity(Move move, float deltaTime)
    {
        var velocity = move.Acceleration * deltaTime * move.transform.right;

        if (velocity.sqrMagnitude > move.MaxSpeed * move.MaxSpeed)
            velocity = velocity.normalized * move.MaxSpeed;

        return velocity;
    }

    private Vector3 GetDecelerationVelocity(Move move, float deltaTime)
    {
        var velocity = Vector3.Lerp(move.Rigidbody.velocity, Vector3.zero, move.Deceleration * deltaTime);

        if (velocity.sqrMagnitude < 0.01)
            velocity = Vector3.zero;

        return velocity;
    }

    private Vector3 GetDirectionalAccelerationVelocity(Move move, float deltaTime)
    {
        var attention = move.Acceleration * deltaTime * move.Direction;
        var velocity = move.Rigidbody.velocity + attention;

        if (velocity.sqrMagnitude > move.MaxSpeed * move.MaxSpeed)
            velocity = velocity.normalized * move.MaxSpeed;

        return velocity;
    }
}
