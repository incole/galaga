﻿using UnityEngine;

[CreateAssetMenu(menuName = "Controllers/Route")]
public class RouteController : Controller<Route>
{
    public override void Update()
    {
        for (int i = 0; i < _items.Count; i++)
        {
            var route = _items[i];

            if (route.CurrentTargetIndex == -1)
                continue;

            //Debug.DrawLine(route.Path[route.CurrentTargetIndex], route.Path[route.CurrentTargetIndex] + Vector3.up, Color.red);
            //Debug.DrawLine(route.Path[route.CurrentTargetIndex], route.transform.position, Color.yellow);

            var direction = route.Path[route.CurrentTargetIndex] - route.transform.position;
            var isLastPoint = route.CurrentTargetIndex == route.Path.Length - 1;

            route.Move.Direction = direction;
            route.Move.SqrDistanceToDestination = direction.sqrMagnitude;

            if (isLastPoint)
            {
                if (direction.sqrMagnitude > .5)
                    continue;

                route.Move.Direction = Vector3.zero;
                route.Move.Rigidbody.velocity = Vector3.zero;
                route.Move.transform.position = route.Path[route.CurrentTargetIndex];
                route.Move.transform.forward = Vector3.back;

                route.Move.enabled = false;

                route.CurrentTargetIndex = -1;
                route.Arrived();

                continue;
            }

            if (direction.sqrMagnitude <= 1)
                route.CurrentTargetIndex++;
        }
    }
}