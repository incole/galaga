﻿using UnityEngine;

[CreateAssetMenu(menuName = "Controllers/Death")]
public class DeathController : Controller<SpaceShip>
{
    public override void Update()
    {
        for (int i = 0; i < _items.Count; i++)
        {
            var spaceShip = _items[i];

            if (!spaceShip.gameObject.activeInHierarchy)
                continue;

            if (spaceShip.Health > 0)
                continue;

            spaceShip.Die();
        }
    }
}
