﻿using UnityEngine;

public class TeleportToScreenViewport: MonoBehaviour
{
    public Vector2 ViewportPosition;

    void Start()
    {
        var camera = Camera.main;
        var targetPlace = ((Vector3)ViewportPosition).WithZ(camera.transform.position.y);

        transform.position = camera.ViewportToWorldPoint(targetPlace);
    }
}