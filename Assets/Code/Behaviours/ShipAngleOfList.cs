﻿using UnityEngine;

public class ShipAngleOfList : MonoBehaviour
{
    public Move Move;
    public float RollMaxValue = 30;
    public float ChangeSpeed = 1;

    private void Update()
    {
        var velocity = Move.Rigidbody.velocity.z;
        var rollValue = velocity / Move.MaxSpeed * RollMaxValue;

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(rollValue, 0, 0), Time.deltaTime * ChangeSpeed);
    }
}
