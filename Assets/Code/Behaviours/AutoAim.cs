﻿using UnityEngine;

[RequireComponent(typeof(Move))]
public class AutoAim: MonoBehaviour, IControllable
{
    [ReadOnly]
    public Move Move;

    [ReadOnly]
    public Transform Target;

    public float AimSpeed = 2;
    public float MaxAngle = 90;

    public int Id { get; set; }

    private void Reset()
    {
        Move = GetComponent<Move>();
    }

    private void Awake()
    {
        if (Move == null)
            Move = GetComponent<Move>();
    }

    public void SetTarget(Transform value)
    {
        Target = value;
    }
}
