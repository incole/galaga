﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Move : MonoBehaviour, IControllable
{
    public int Id { get; set; }
    public MoveType Type = MoveType.Directional;
    public float MaxSpeed = 0.3f;

    public float Acceleration = 1;
    public float Deceleration = 1;

    public AnimationCurve RotationSpeedOverDistance;

    [HideInInspector]
    public float SqrDistanceToDestination;

    [ReadOnly]
    public Rigidbody Rigidbody;

    [ReadOnly]
    public Vector3 Direction;

    [ReadOnly]
    public Vector3 PreviousPosition;

    private void Reset()
    {
        PreviousPosition = transform.position;
        Direction = Vector3.zero;
        Rigidbody = GetComponent<Rigidbody>();
        Rigidbody.isKinematic = true;
    }

    private void Awake()
    {
        if (Rigidbody == null)
            Rigidbody = GetComponent<Rigidbody>();
    }
}

public enum MoveType
{
    Forward,
    Directional
}