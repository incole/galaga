﻿using UnityEngine;

[RequireComponent(typeof(SpaceShip))]
public class ShipsCollisionHandler: MonoBehaviour
{
    public LayerMask LayerMask;

    private SpaceShip _spaceShip;

    public void Awake()
    {
        _spaceShip = GetComponent<SpaceShip>();
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (!LayerMask.ContainsLayer(collision.gameObject.layer))
            return;

        var spaceShip = collision.gameObject.GetComponent<SpaceShip>();

        if (spaceShip != null)
            spaceShip.ApplyDamage(spaceShip.Health);

        _spaceShip.ApplyDamage(_spaceShip.Health);
    }
}