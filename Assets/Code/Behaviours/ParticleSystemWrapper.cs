﻿using System;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemWrapper : MonoBehaviour
{
    public event Action<ParticleSystemWrapper> OnParticleSystemFinished;

    public ParticleSystem ParticleSystem { get; private set; }

    void Awake()
    {
        ParticleSystem = GetComponent<ParticleSystem>();
        
        var main = ParticleSystem.main;
        main.stopAction = ParticleSystemStopAction.Callback;
    }

    void OnParticleSystemStopped()
    {
        OnParticleSystemFinished?.Invoke(this);
    }
}