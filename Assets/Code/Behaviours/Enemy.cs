﻿using System.Threading.Tasks;
using UnityEngine;

public class Enemy: MonoBehaviour
{
    public SpaceShip SpaceShip;
    public Weapon Weapon;
    public Route Route;

    public Transform Target;

    [Range(0f, 20)]
    public float MissRadius = 5;

    private void Awake()
    {
        Route.OnDestinationAchived += OnArrived;
    }

    private void OnArrived(Route route)
    {
        enabled = true;
    }

    private void Update()
    {
        if (Weapon.IsCooling)
            return;

        if (Physics.Raycast(transform.position, Target.position - transform.position, 5, Weapon.ProjectilePool.Prefab.HitboxMask, QueryTriggerInteraction.Collide))
            return;

        if (!Target.gameObject.activeInHierarchy)
            return;

        // TODO: fix implicit approach

        if (MissRadius == 0)
        {
            Weapon.Fire(SpaceShip, Target);
            return;
        }

        var target = new Vector3
        (
            Target.position.x + Random.Range(-MissRadius, MissRadius),
            Target.position.y,
            Target.position.z + Random.Range(-MissRadius, MissRadius)
        );

        Weapon.Fire(SpaceShip, target - transform.position);
    }
}