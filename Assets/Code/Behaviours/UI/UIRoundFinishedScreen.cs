﻿using UnityEngine;
using UnityEngine.UI;

public class UIRoundFinishedScreen: MonoBehaviour
{
    public Text Score;
    public UIFader Fader;

    public void Show(int score)
    {
        Score.text = score.ToString();

        Fader.FadeInAsync();
    }
}
