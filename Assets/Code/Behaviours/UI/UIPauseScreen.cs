﻿using UnityEngine;

public class UIPauseScreen : MonoBehaviour
{
    public UIFader Fader;
    public KeyCode PauseKey;

    public bool IsPaused = false;

    public void Update()
    {
        if (!Input.GetKeyUp(PauseKey))
            return;

        Toggle();
    }

    public void Toggle()
    {
        IsPaused = !IsPaused;

        if (IsPaused)
        {
            Time.timeScale = 0;
            Fader.FadeInAsync();
        }
        else
        {
            Time.timeScale = 1;
            Fader.FadeOutAsync();
        }
    }
}