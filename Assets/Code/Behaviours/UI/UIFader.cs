﻿using Cysharp.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class UIFader : MonoBehaviour
{
    public CanvasGroup UiElement;
    public float Speed = 1f;
    public bool UnscaledTime = false;

    public bool ControlsRaycast = true;
    public bool ControlsInteractable = true;

    private CancellationTokenSource _cancellationTokenSource;

    public float TargetAlpha { get; private set; }

    public void Toggle()
    {
        if (TargetAlpha == 1)
            FadeOutAsync();
        else
            FadeInAsync();
    }

    public async UniTask FadeInAsync(float speed)
    {
        Speed = speed;
        TargetAlpha = 1;

        await FadeCanvasGroupAsync();
    }

    public async UniTask FadeOutAsync(float speed)
    {
        Speed = speed;
        TargetAlpha = 0;

        await FadeCanvasGroupAsync();
    }

    public async void FadeInAsync()
    {
        await FadeInAsync(Speed);
    }

    public async void FadeOutAsync()
    {
        await FadeOutAsync(Speed);
    }

    public async UniTask FadeCanvasGroupAsync()
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();

        if (UiElement.alpha == TargetAlpha)
            return;

        _cancellationTokenSource = new CancellationTokenSource();

        var token = _cancellationTokenSource;

        while (UiElement.alpha != TargetAlpha)
        {
            var deltaTime = UnscaledTime ? Time.unscaledTime : Time.deltaTime;

            ApplyOpacity(deltaTime * Speed);

            if (Mathf.Abs(UiElement.alpha - TargetAlpha) <= 0.05f)
            {
                ApplyOpacity(1);

                _cancellationTokenSource = null;
                return;
            }

            await UniTask.NextFrame();

            if (token.IsCancellationRequested)
                return;
        }

        _cancellationTokenSource = null;
    }

    private void ApplyOpacity(float t)
    {
        UiElement.alpha = Mathf.Lerp(UiElement.alpha, TargetAlpha, t);

        if (ControlsRaycast) UiElement.blocksRaycasts = UiElement.alpha == 1;
        if (ControlsInteractable) UiElement.interactable = UiElement.alpha == 1;
    }
}
