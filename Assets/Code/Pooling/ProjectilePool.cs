﻿using UnityEngine;

[CreateAssetMenu(menuName = "Pools/Projectile")]
public class ProjectilePool : ScriptablePool<Projectile> 
{
    public MoveController MoveController;
    public AutoAimController AutoAimController;

    public override void Initialize()
    {
        base.Initialize();

        OnGet += ProjectilePool_OnGet;
        OnRelease += ProjectilePool_OnRelease;
    }

    private void ProjectilePool_OnGet(Projectile obj)
    {
        MoveController.Register(obj.Move);
        
        if (obj is HomingProjectile)
        {
            var hobj = obj as HomingProjectile;
            AutoAimController.Register(hobj.Aim);
        }
    }

    private void ProjectilePool_OnRelease(Projectile obj)
    {
        MoveController.Release(obj.Move);

        if (obj is HomingProjectile)
        {
            var hobj = obj as HomingProjectile;
            AutoAimController.Release(hobj.Aim);
        }
    }
}

/*
{
public SpaceShip[] Archtypes;
public Projectile[] ProjectileTypes;
public Dictionary<Projectile, LinkedPool<Projectile>> Pools { get; private set; }

public void Initialize()
{
    var pools = new ProjectilePools();
    Pools = new Dictionary<Projectile, LinkedPool<Projectile>>();

    var root = new GameObject($"Projectile Pools");

    for (int i = 0; i < ProjectileTypes.Length; i++)
    {
        var projectileType = ProjectileTypes[i];

        var parent = new GameObject($"{projectileType.gameObject.name} Pool");
        parent.transform.parent = root.transform;

        var projectilePool = new LinkedPool<Projectile>
        (
            createFunc: () =>
            {
                var projectileObejct = GameObject.Instantiate(projectileType.gameObject, parent.transform);
                var projectile = projectileObejct.GetComponent<Projectile>();


                return projectile;
            },

            actionOnGet: (obj) => obj.gameObject.SetActive(true),
            actionOnRelease: (obj) => obj.gameObject.SetActive(false),
            actionOnDestroy: (obj) => Destroy(obj),
            maxSize: 100
        );

        Pools.Add(projectileType, projectilePool);
    }
}
}

*/