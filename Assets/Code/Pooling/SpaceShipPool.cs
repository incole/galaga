﻿using UnityEngine;

[CreateAssetMenu(menuName = "Pools/Space Ship")]
public class SpaceShipPool : ScriptablePool<SpaceShip>
{
    public MoveController MoveController;
    public RouteController RouteController;
    public DeathController DeathController;

    public override void Initialize()
    {
        base.Initialize();

        OnGet += SpaceShipPool_OnGet;
        OnRelease += SpaceShipPool_OnRelease;
    }

    private void SpaceShipPool_OnGet(SpaceShip obj)
    {
        MoveController.Register(obj.Move);

        if (obj.Route != null)
            RouteController.Register(obj.Route);

        DeathController.Register(obj);
    }

    private void SpaceShipPool_OnRelease(SpaceShip obj)
    {
        MoveController.Release(obj.Move);

        if (obj.Route != null)
            RouteController.Release(obj.Route);

        DeathController.Release(obj);
    }
}


/*
{
public SpaceShip[] Archtypes;
public Projectile[] ProjectileTypes;
public Dictionary<Projectile, LinkedPool<Projectile>> Pools { get; private set; }

public void Initialize()
{
    var pools = new ProjectilePools();
    Pools = new Dictionary<Projectile, LinkedPool<Projectile>>();

    var root = new GameObject($"Projectile Pools");

    for (int i = 0; i < ProjectileTypes.Length; i++)
    {
        var projectileType = ProjectileTypes[i];

        var parent = new GameObject($"{projectileType.gameObject.name} Pool");
        parent.transform.parent = root.transform;

        var projectilePool = new LinkedPool<Projectile>
        (
            createFunc: () =>
            {
                var projectileObejct = GameObject.Instantiate(projectileType.gameObject, parent.transform);
                var projectile = projectileObejct.GetComponent<Projectile>();


                return projectile;
            },

            actionOnGet: (obj) => obj.gameObject.SetActive(true),
            actionOnRelease: (obj) => obj.gameObject.SetActive(false),
            actionOnDestroy: (obj) => Destroy(obj),
            maxSize: 100
        );

        Pools.Add(projectileType, projectilePool);
    }
}
}

*/