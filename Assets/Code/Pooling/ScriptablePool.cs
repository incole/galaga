﻿using System;
using UnityEngine;
using UnityEngine.Pool;

public class ScriptablePool<T> : InitilizableScriptable, IPool<T> where T : Component
{
    public T Prefab;
    public int MaxSize = 50;
    public Transform Root { get; private set; }
    private LinkedPool<T> _items;

    public event Action<T> OnCreate;
    public event Action<T> OnGet;
    public event Action<T> OnRelease;

    public override void Initialize()
    {
        var root = new GameObject($"Pool for {Prefab.name}");

        var parent = new GameObject($"{Prefab.name} Pool");
        parent.transform.parent = root.transform;

        _items = new LinkedPool<T>
        (
            createFunc: () => 
            {
                var obj = GameObject.Instantiate(Prefab, parent.transform);
                OnCreate?.Invoke(obj);
                return obj;
            },
            actionOnGet: (obj) =>
            {
                obj.gameObject.SetActive(true);
                OnGet?.Invoke(obj);
            },
            actionOnRelease: (obj) =>
            {
                obj.gameObject.SetActive(false);
                OnRelease?.Invoke(obj);
            },
            actionOnDestroy: (obj) => GameObject.Destroy(obj.gameObject),
            maxSize: MaxSize
        );

        OnCreate = null;
        OnGet = null;
        OnRelease = null;
    }

    public T Get() => _items.Get();
    public void Release(T obj) => _items.Release(obj);
}