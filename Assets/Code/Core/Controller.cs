﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Controller<T>: Controller where T : IControllable
{
    const int DefaultListSize = 1024;

    internal List<T> _items;
    private int _counter;

    public void Register(T value)
    {
        _items.Add(value);
        value.Id = _counter;

        _counter++;
    }

    public void Release(T value)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i].Id != value.Id)
                continue;

            _items.RemoveAt(i);
            return;
        }
    }

    public override void Init()
    {
        _items = new List<T>();
        _counter = 0;
    }
}

public abstract class Controller : ScriptableObject
{
    public abstract void Update();
    public abstract void Init();
}

public interface IControllable
{
    public int Id { get; set; }
}