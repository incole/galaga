﻿using UnityEngine;

public interface IInitilizable
{
    public void Initialize();
}

public abstract class InitilizableScriptable : ScriptableObject, IInitilizable
{
    public virtual void Initialize() { }
}